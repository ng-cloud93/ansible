#!/usr/bin/env python3

import argparse
import ast
import os
import re
import requests
import sys
from time import time

try:
    import ConfigParser
except ImportError:
    import configparser as ConfigParser

import json


class DoManager:
    def __init__(self, api_token):
        self.api_token = api_token
        self.api_endpoint = 'https://api.digitalocean.com/v2'
        self.headers = {'Authorization': 'Bearer {0}'.format(self.api_token),
                        'Content-type': 'application/json'}
        self.timeout = 60

    def _url_builder(self, path):
        if path[0] == '/':
            path = path[1:]
        return '%s/%s' % (self.api_endpoint, path)

    def send(self, url, method='GET', data=None):
        url = self._url_builder(url)
        data = json.dumps(data)
        try:
            if method == 'GET':
                resp_data = {}
                incomplete = True
                while incomplete:
                    resp = requests.get(url, data=data, headers=self.headers, timeout=self.timeout)
                    json_resp = resp.json()

                    for key, value in json_resp.items():
                        if isinstance(value, list) and key in resp_data:
                            resp_data[key] += value
                        else:
                            resp_data[key] = value

                    try:
                        url = json_resp['links']['pages']['next']
                    except KeyError:
                        incomplete = False

        except ValueError as e:
            sys.exit("Unable to parse result from %s: %s" % (url, e))
        return resp_data

    def all_active_droplets(self):
        resp = self.send('droplets/')
        return resp['droplets']

    def all_regions(self):
        resp = self.send('regions/')
        return resp['regions']

    def all_images(self, filter_name='global'):
        params = {'filter': filter_name}
        resp = self.send('images/', data=params)
        return resp['images']

    def sizes(self):
        resp = self.send('sizes/')
        return resp['sizes']

    def all_ssh_keys(self):
        resp = self.send('account/keys')
        return resp['ssh_keys']

    def all_domains(self):
        resp = self.send('domains/')
        return resp['domains']

    def show_droplet(self, droplet_id):
        resp = self.send('droplets/%s' % droplet_id)
        return resp['droplet']

    def all_tags(self):
        resp = self.send('tags')
        return resp['tags']


dirname = os.path.abspath(os.path.dirname(__file__))
default_config_file = os.path.join(dirname, 'hosts.ini')


def make_config_parser(filename=default_config_file):
    config_parser = ConfigParser.ConfigParser()
    config_parser.read(filename)
    return config_parser


def get_api_token_from_ini():
    config_parser = make_config_parser()
    digital_ocean = config_parser['digital-ocean']
    return digital_ocean['api_token']


def get_ansible_ssh_port_from_ini():
    config_parser = make_config_parser()
    ansible = config_parser['ansible']
    return ansible['ssh_port']


def get_inventory_from_digital_ocean(api_token):
    manager = DoManager(api_token)

    inventory_data = {}
    for tag in manager.all_tags():
        tag_name = tag['name']
        inventory_data[tag_name] = {}
        inventory_data[tag_name]['hosts'] = []

    ansible_ssh_port = get_ansible_ssh_port_from_ini()
    for droplet in manager.all_active_droplets():
        droplet_name = droplet['name']
        for tag in droplet['tags']:
            inventory_data[tag]['hosts'].append(droplet_name)

    return inventory_data


def get_parser():
    parser = argparse.ArgumentParser(prog=__name__)
    parser.add_argument('--list', action='store_true')
    parser.add_argument('--host', default='')
    return parser


def main():
    api_token = get_api_token_from_ini()
    inventory_data = get_inventory_from_digital_ocean(api_token)

    parser = get_parser()
    opts = parser.parse_args()
    if opts.host:
        keys = list(inventory_data.keys())
        if opts.host in keys:
            for key in keys:
                if key.lower() != opts.host:
                    inventory_data.pop(key)
        else:
            host = opts.host
            inventory_data[host] = {}

    json_data = json.dumps(inventory_data, indent=2)
    print(json_data)


if __name__ == "__main__":
    main()
